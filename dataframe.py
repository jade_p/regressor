import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats
from scipy.stats import norm, skew

from sklearn.preprocessing import LabelEncoder
from scipy.special import boxcox1p


class Preprocessing:

    def __init__(self, path):
        self.table = pd.read_csv(path)
        self.variables = self.table.drop(self.table.columns[-1], axis=1)
        self.y = self.table.iloc[:, -1]
        self.length = self.variables.shape[0]
        self.numeric_data = self.variables.select_dtypes(include=[np.number])
        self.categorical_data = self.variables.select_dtypes(exclude=[np.number])

    def remove_outiers(self):
        self.table = self.table.drop(self.table[(self.table['GrLivArea'] > 4000) & (self.table['SalePrice'] < 300000)].index)
        self.variables = self.table.drop(self.table.columns[-1], axis=1)
        self.y = self.table.iloc[:, -1]

    def log_transfo_output(self):
        self.y = np.log1p(self.y)

    def missing_ratio(self):
        total_na = (self.variables.isnull().sum() / len(self.variables)) * 100
        total_na = total_na.drop(total_na[total_na == 0].index).sort_values(ascending=False)[:30]
        missing_data = pd.DataFrame({'Missing Ratio' :total_na})
        print(missing_data.head(20))
        None_col = ["PoolQC", "MiscFeature", "Alley", "Fence", "FireplaceQu", "GarageType", "GarageFinish", "GarageQual",
                 "GarageCond", 'BsmtQual', 'BsmtCond', 'BsmtExposure', 'BsmtFinType1', 'BsmtFinType2', "MasVnrType",
                 "MSSubClass"]

        for missing_col in None_col:
            self.variables[missing_col] = self.variables[missing_col].fillna("None")
        Zeros = ['GarageYrBlt', 'GarageArea', 'GarageCars', 'BsmtFinSF1', 'BsmtFinSF2', 'BsmtUnfSF','TotalBsmtSF',
                 'BsmtFullBath', 'BsmtHalfBath', "MasVnrArea"]
        for missing_col in Zeros:
            self.variables[missing_col] = self.variables[missing_col].fillna(0)

        self.variables["LotFrontage"] = self.variables.groupby("Neighborhood")["LotFrontage"].transform(
            lambda x: x.fillna(x.median()))

        Modes = ["MSZoning", 'Electrical', 'KitchenQual', "Exterior1st", "Exterior2nd", "SaleType"]
        for missing_col in Modes:
            self.variables[missing_col] = self.variables[missing_col].fillna(self.variables[missing_col].mode()[0])

        self.variables["Functional"] = self.variables["Functional"].fillna("Typ")

        self.variables = self.variables.drop(['Utilities'], axis=1)

    def rectify_numeric(self):
        #print("The columns of numeric data are the following "+str(self.numeric_data.columns))
        #categorical_columns = str(input("Please input the column names which have to be switched to categorical "
        #                                "data instead, separated by a space :")).split()
        categorical_columns = ("MSSubClass", "OverallCond", "YrSold", "MoSold")
        print(categorical_columns)
        for categorical in categorical_columns:
            try:
                self.variables[categorical] = self.variables[categorical].astype(str)
            except KeyError:
                renamed_variable = str(input("The {} variable was not found, please check spelling and reenter it correctly".format(categorical)))
                self.variables[renamed_variable] = self.variables[renamed_variable].astype(str)

    def rectify_categorical(self):
        #print("The columns of categorical data are the following " + str(self.categorical_data.columns))
        #numeric_columns = str(input("Please input the column names which have to be Label encoded "
        #                                "(order matter), separated by a space :")).split()
        numeric_columns = ('FireplaceQu', 'BsmtQual', 'BsmtCond', 'GarageQual', 'GarageCond',
                'ExterQual', 'ExterCond', 'HeatingQC', 'PoolQC', 'KitchenQual', 'BsmtFinType1',
                'BsmtFinType2', 'Functional', 'Fence', 'BsmtExposure', 'GarageFinish', 'LandSlope',
                'LotShape', 'PavedDrive', 'Street', 'Alley', 'CentralAir', 'MSSubClass', 'OverallCond',
                'YrSold', 'MoSold')
        print(numeric_columns)
        for numeric in numeric_columns:
            try:
                lbl = LabelEncoder()
                lbl.fit(list(self.variables[numeric].values))
                self.variables[numeric] = lbl.transform(list(self.variables[numeric].values))
            except KeyError:
                renamed_variable = str(input("The {} variable was not found, please check spelling and reenter it correctly".format(numeric)))
                self.variables[renamed_variable] = lbl.transform(list(self.variables[renamed_variable].values))
        print("The shape of the table is : {}".format(self.variables.shape))

    def numeric_skew(self, threshold=0.75, lam=0.15):
        numeric_features = self.variables.dtypes[self.variables.dtypes != "object"].index
        skewed_features = self.variables[numeric_features].apply(lambda x: skew(x.dropna())).sort_values(ascending=False)
        print("Skew in numerical features: \n")
        skewness = pd.DataFrame({'Skew' : skewed_features})
        print(skewness.head(20))
        skewness = skewness[abs(skewness) > threshold]
        print("There are {} skewed numerical features to Box Cox transform".format(skewness.shape[0]))
        skewed_features = skewness.index
        for feature in skewed_features:
            self.variables[feature] = boxcox1p(self.variables[feature], lam)

    def to_dummy(self):
        self.variables = pd.get_dummies(self.variables)