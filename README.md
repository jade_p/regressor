# Regressor

This Machine Learning Project aims at creating a semi-automated regressor for tabular data analysis and regression.
User has to input some information to confirm or infer the assumptions taken by the model.
Features that need to be checked with operator are : 
- Values to replace NA values per category type (num vs cat) - possibility of custom value pr column
- Confirmation of numerical vs categorical data types
- Success metric (default to RMSE)
The Preprocessing steps that are performed are the following :
- LabelEncoding of some categorical variables / OneHot
- Normalization of skewed numerical variables
- Replacement of NA values
- Dummied categorical variables

The algorithm then performs the regression on the preprocessed dataset and returns a table with the regression scores from the following regression algorithms :
- Lasso Regression / Robust Scaler
- ElasticNet / Robust Scaler
- Kernek Ridge Regressor / Polynomial 
- Gradient Boosting Regression / huber loss
- XGBoost
- Light GBM

It then returns the best algorithm and the associated success score.
UC (under construction)
Next step : auto-tuning of models