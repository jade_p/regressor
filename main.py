import pandas as pd
import numpy as np
from dataframe import Preprocessing
from model import Regressor

def main():
    table_name = 'train.csv'
    table = Preprocessing(table_name)
    table.remove_outiers()
    table.log_transfo_output()
    table.missing_ratio()
    table.numeric_skew()
    table.rectify_numeric()
    table.rectify_categorical()
    table.to_dummy()
    table.numeric_skew()
    regressor = Regressor()
    print(table.y)
    regressor.regress(table.variables, table.y)

if __name__ == "__main__":
    main()