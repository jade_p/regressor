from sklearn.linear_model import ElasticNet, Lasso,  BayesianRidge, LassoLarsIC
from sklearn.ensemble import RandomForestRegressor,  GradientBoostingRegressor
from sklearn.kernel_ridge import KernelRidge
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import RobustScaler
from sklearn.base import BaseEstimator, TransformerMixin, RegressorMixin, clone
from sklearn.model_selection import KFold, cross_val_score, train_test_split
from sklearn.metrics import mean_squared_error
import xgboost as xgb
import lightgbm as lgb
import numpy as np

class Regressor:
    def __init__(self):
        self.lasso = make_pipeline(RobustScaler(), Lasso(alpha=0.0005, random_state=1))
        self.ENet = make_pipeline(RobustScaler(), ElasticNet(alpha=0.0005, l1_ratio=.9, random_state=3))
        self.KRR = KernelRidge(alpha=0.6, kernel='polynomial', degree=2, coef0=2.5)
        self.model_GB = GradientBoostingRegressor(n_estimators=3000, learning_rate=0.05, max_depth=4, max_features='sqrt',
                                             min_samples_leaf=15, min_samples_split=10, loss='huber', random_state=5)
        self.model_XGB = xgb.XGBRegressor(colsample_bytree=0.4603, gamma=0.0468, learning_rate=0.05, max_depth=3,
                                     min_child_weight=1.7817, n_estimators=2200, reg_alpha=0.4640, reg_lambda=0.8571,
                                     subsample=0.5213, silent=1, random_state=7, nthread=-1)
        self.model_LGB = lgb.LGBMRegressor(objective='regression', num_leaves=5, learning_rate=0.05, n_estimators=720,
                                      max_bin=55, bagging_fraction=0.8, bagging_freq=5, feature_fraction=0.2319,
                                      feature_fraction_seed=9, bagging_seed=9, min_data_in_leaf=6,
                                      min_sum_hessian_in_leaf=11)
        self.models_list = [self.lasso, self.ENet, self.KRR, self.model_GB, self.model_XGB, self.model_LGB]


    def rmsle_cv(self, model, train, y_train, n_folds=5):
        y_train = np.array(y_train.values)
        train = np.array(train.values)
        #print(y_train.type)
        kf = KFold(n_folds, shuffle=True, random_state=42).get_n_splits(train)
        rmse = np.sqrt(-cross_val_score(model, train, y_train, scoring="neg_mean_squared_error", cv = kf))
        return rmse


    def regress(self, train, y_train):
        for model in self.models_list:
            score = self.rmsle_cv(model, train, y_train)
            print("The {} model RMSE mean score and std: {:.4f} ({:.4f})\n".format(str(model), score.mean(), score.std()))